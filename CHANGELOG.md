# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-18)

### Edited

* Build the feature hierarchy.

### Fixed

* Edit all locus_tag: EBI prefix replacement yields to a huge number of duplicated locus_tag values.

## v1.0 (2021-05-18)

### Added

* The 27 annotated scaffolds of Zygosaccharomyces bailii CLIB 213 (source EBI, [GCA_000442885.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000442885.1)).
