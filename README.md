## *Zygosaccharomyces bailii* CLIB 213

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB1908](https://www.ebi.ac.uk/ena/browser/view/PRJEB1908)
* **Assembly accession**: [GCA_000442885.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000442885.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: ZYBA0
* **Assembly length**: 10,268,813
* **#Scaffolds**: 27
* **Mitochondiral**: No
* **N50 (L50)**: 932,251 (4)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 4941
* **Pseudogene count**: 218
* **tRNA count**: 162
* **rRNA count**: 0
* **Mobile element count**: 0
